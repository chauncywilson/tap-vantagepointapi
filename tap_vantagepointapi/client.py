"""REST client handling, including vantagepointAPIStream base class."""

from __future__ import annotations
import datetime

import sys
from pathlib import Path
from typing import Any, Callable, Iterable, Dict,Optional

import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.pagination import BaseAPIPaginator  # noqa: TCH002
from singer_sdk.streams import RESTStream

from tap_vantagepointapi.auth import vantagepointAPIAuthenticator

if sys.version_info >= (3, 8):
    from functools import cached_property
else:
    from cached_property import cached_property

_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]
import backoff
from requests.exceptions import HTTPError
from singer_sdk import typing as th
import requests


class vantagepointAPIStream(RESTStream):
    """vantagepointAPI stream class."""
    schema_response = None
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["url"]

    records_jsonpath = "$[*]"  # Or override `parse_response`.

    # Set this value or override `get_new_paginator`.
    next_page_token_jsonpath = "$.next_page"  # noqa: S105

    @cached_property
    def authenticator(self) -> _Auth:
        """Return a new authenticator object.

        Returns:
            An authenticator instance.
        """
        return vantagepointAPIAuthenticator.create_for_stream(self, self.config["url"])

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed.

        Returns:
            A dictionary of HTTP headers.
        """
        time = datetime.datetime.now().strftime("%a, %d %b %Y %H:%M:%S GMT")
        headers = {
            "Content-Type": "application/json",
            "Date": time,
        }
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        data = response.json()
        if previous_token is None:
            return 2

        if len(data)>0:
            return previous_token + 1

        return None

    def get_url_params(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ANN401
    ) -> dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization.

        Args:
            context: The stream context.
            next_page_token: The next page index or value.

        Returns:
            A dictionary of URL query parameters.
        """
        params: dict = {
            "limit"  : self.config.get("pageSize", 1000), # 2,500 is the max page size, default is 1000["pageSize"],
        }
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key
        return params

    @backoff.on_exception(backoff.expo, HTTPError, max_tries=5, factor=2)
    def get_schema(self):
        self.logger.info(f"Getting schema for {self.name}")
        url = f"{self.url_base}/metadata{self.path}"
        headers = self.http_headers
        auth_headers = self.authenticator.auth_headers
        headers.update(auth_headers)
        response = requests.get(url=url,headers=headers)
        response.raise_for_status()
        self.schema_response = response.json()

    @property
    def schema(self):
        # Get schema for the table
        if self.schema_response is None:
            self.get_schema()

        response = self.schema_response
        properties_list = []
        for table_key in response.keys():
            if "Fields" in response[table_key]:
                for field in response[table_key]['Fields']:
                    property_name = field['FieldName']
                    field_type = field['FieldType']
                    if field_type == 'Numeric':
                        properties_list.append(th.Property(property_name, th.NumberType))
                    elif field_type == 'Datetime':
                        properties_list.append(th.Property(property_name, th.DateTimeType))
                    elif field_type == 'Date':
                        properties_list.append(th.Property(property_name, th.DateType))
                    else:
                        properties_list.append(th.Property(property_name, th.StringType))
        return th.PropertiesList(*properties_list).to_dict()   

    def process_row_types(self,row) -> Dict[str, Any]:
        schema = self.schema['properties']
        for field, value in row.items():
            if field not in schema:
                # Skip fields not found in the schema
                continue

            field_info = schema[field]
            field_type = field_info.get("type", ["null"])[0]
            field_format = field_info.get("format", None)

            if value is None:
                continue

            if field_type == "string" and field_format == "date-time":
                try:
                    # Attempt to parse string as date-time
                    # If successful, no need to cast
                    if value:
                        _ = datetime.datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fZ")
                except ValueError:
                    # If parsing fails, consider it as a type mismatch and attempt to cast
                    try:
                        row[field] = datetime.datetime.fromisoformat(value)
                    except ValueError:
                        # No need to raise an error, just continue with the loop
                        continue

            elif field_type == "boolean":
                if not isinstance(value, bool):
                    # Attempt to cast to boolean
                    if value.lower() == "true":
                        row[field] = True
                    elif value.lower() == "false":
                        row[field] = False
                    else:
                        # No need to raise an error, just continue with the loop
                        continue

            elif field_type == "number":
                if isinstance(value, str):
                    # Attempt to cast to float only if the value is a string with decimals
                    if "." in value:
                        try:
                            row[field] = float(value)
                        except ValueError:
                            # Set default value for float type
                            row[field] = 0.0
                    else:
                        # Attempt to cast to int if there are no decimals
                        try:
                            row[field] = int(value)
                        except ValueError:
                            # Set default value for int type
                            row[field] = 0

            elif field_type == "string":
                if not isinstance(value, str):
                    # Attempt to cast to string
                    row[field] = str(value)

            else:
                # Unsupported type
                # No need to raise an error, just continue with the loop
                continue

        return row 
    
    def post_process(
        self,
        row: dict,
        context: dict | None = None,  # noqa: ARG002
    ) -> dict | None:
        row = self.process_row_types(row)  
        return row